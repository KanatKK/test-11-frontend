import React from 'react';
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {addCategory, addDescription, addImage, addImageName, addNewItem, addPrice, addTitle} from "../../store/actions";

const CreateItem = props => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.user.user);
    const itemData = useSelector(state => state.addItem);
    const title = useSelector(state => state.addItem.title);
    const description = useSelector(state => state.addItem.description);
    const price = useSelector(state => state.addItem.price);
    const imageName = useSelector(state => state.addItem.imageName);
    const category = useSelector(state => state.addItem.category);

    const addTitleHandler = event => {
        dispatch(addTitle(event.target.value));
    };

    const addDescriptionHandler = event => {
        dispatch(addDescription(event.target.value));
    };

    const addPriceHandler = event => {
        dispatch(addPrice(event.target.value));
    };

    const addCategoryHandler = event => {
        dispatch(addCategory(event.target.value));
    };

    const addImageHandler = event => {
        if (event.target.files[0] !== undefined) {
            dispatch(addImageName(event.target.files[0].name));
            dispatch(addImage(event.target.files[0]));
        } else {
            dispatch(addImage(null));
            dispatch(addImageName("Choose an image..."));
        }
    };

    const send = async event => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(itemData).forEach(key => {
            if (key !== 'imageName') {
                formData.append(key, itemData[key]);
            }
        });
        const headers = {
            'Authentication': user.token,
        };
        await addNewItem(headers, formData);
        dispatch(addTitle(""));
        dispatch(addDescription(""));
        dispatch(addPrice(null));
        dispatch(addCategory("Cars"));
        dispatch(addImage(null));
        dispatch(addImageName("Choose an image..."));
        props.history.push("/");
    };

    return (
        <div className="container">
            <header>
                <h2>Create Item</h2>
                <span className="nav">
                    <NavLink
                        style={{
                            color: 'black',
                            textDecoration: 'underline',
                            cursor: 'pointer',
                            marginLeft: 5
                        }}
                        to="/"
                    >
                    Main Page
                </NavLink>
                </span>
            </header>
            <div className="content">
                <input
                    type="text" value={title} onChange={addTitleHandler}
                    className="createItemField" placeholder="Title"
                />
                <input
                    type="text" value={description} onChange={addDescriptionHandler}
                    className="createItemField" placeholder="Description"
                />
                <input
                    type="text" value={price} onChange={addPriceHandler}
                    className="createItemField" placeholder="Price"
                />
                <label className="inputFileLabel" htmlFor="inputFile">{imageName}</label>
                <input
                    type="file" className="chooseImage" onChange={addImageHandler}
                    id="inputFile" accept=".jpg, .jpeg, .png"
                />
                <div className="categoriesBloc">
                    <label htmlFor="category">Choose category: </label>
                    <select onChange={addCategoryHandler} value={category} name="category" className="categoryChanger" placeholder="select">
                        <option value="Cars">Cars</option>
                        <option value="Computers">Computers</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
                <button className="addButton" onClick={send}>Create</button>
            </div>
        </div>
    );
};

export default CreateItem;