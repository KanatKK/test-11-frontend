import React from 'react';
import './App.css';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import MainPage from "../mainPage/mainPage";
import SignUp from "../../components/Registration/SignUp";
import Login from "../../components/Registration/Login";
import CreateItem from "../../components/Item/CreateItem";
import Item from "../../components/Item/Item";

const App = () => {
  return (
      <BrowserRouter>
          <Switch>
              <Route path="/" exact component={MainPage}/>
              <Route path="/register" exact component={SignUp}/>
              <Route path="/login" exact component={Login}/>
              <Route path="/createItem" exact component={CreateItem}/>
              <Route path="/item/:id" exact component={Item}/>
          </Switch>
      </BrowserRouter>
  );
};

export default App;
