import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addDisplayName, addPassword, addPhoneNumber, addUsername, createUser} from "../../store/actions";
import {NavLink} from "react-router-dom";

const SignUp = props => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.user.user);
    const username = useSelector(state => state.addUser.username);
    const password = useSelector(state => state.addUser.password);
    const displayName = useSelector(state => state.addUser.displayName);
    const phoneNumber = useSelector(state => state.addUser.phoneNumber);
    const userData = useSelector(state=> state.addUser);

    const toLogIn = () => {
        props.history.push("/login");
    };

    const addUserNameHandler = event => {
        dispatch(addUsername(event.target.value));
    };

    const addPasswordHandler = event => {
        dispatch(addPassword(event.target.value));
    };

    const addPhoneNumberHandler = event => {
        dispatch(addPhoneNumber(event.target.value));
    };

    const addDisplayNameHandler = event => {
        dispatch(addDisplayName(event.target.value));
    };

    const createUserHandler = () => {
        dispatch(createUser(userData));
        dispatch(addUsername(""));
        dispatch(addPassword(""));
        dispatch(addPhoneNumber(""));
        dispatch(addDisplayName(""));
    };

    if (user !== null && user.username) {
        props.history.push("/");
    }

    return (
        <div className="container">
            <header>
                <h2>Registration</h2>
                <span className="nav">
                    <NavLink
                        style={{
                            color: 'black',
                            textDecoration: 'underline',
                            cursor: 'pointer'
                        }}
                        to="/"
                    >
                        MainPage
                    </NavLink>
                </span>
            </header>
            <div className="registration">
                <input
                    type="text" placeholder="Username" className="registrationField"
                    onChange={addUserNameHandler} value={username}
                />
                <input
                    type="text" placeholder="Display Name" className="registrationField"
                    onChange={addDisplayNameHandler} value={displayName}
                />
                <input
                    type="text" placeholder="Phone number" className="registrationField"
                    onChange={addPhoneNumberHandler} value={phoneNumber}
                />
                <input
                    type="text" placeholder="Password" className="registrationField"
                    onChange={addPasswordHandler} value={password}
                />
                <button type="button" onClick={createUserHandler} className="regButton">Register</button>
                {user && <span className="error">{user.error}</span>}
                <p onClick={toLogIn}>Already have an account? Login!</p>
            </div>
        </div>
    );
};

export default SignUp;