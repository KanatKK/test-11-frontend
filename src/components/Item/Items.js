import React from 'react';
import {NavLink} from "react-router-dom";

const Items = props => {
    return (
        <div className="item">
            <div className="itemImage">
                <img src={'http://localhost:8000/uploads/' + props.image} alt=""/>
            </div>
            <div className="itemInfo">
                <h3>
                    <NavLink
                        style={{
                            color: "black"
                        }}
                        to={"/item/" + props.id}
                    >
                        {props.title}
                    </NavLink>
                </h3>
                <p className="price">{props.price} $</p>
            </div>
        </div>
    );
};

export default Items;