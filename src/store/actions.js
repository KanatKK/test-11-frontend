import axios from "axios";
import {
    ADD_CATEGORY,
    ADD_DESCRIPTION,
    ADD_DISPLAY_NAME, ADD_IMAGE, ADD_IMAGE_NAME,
    ADD_PASSWORD,
    ADD_PHONE_NUMBER, ADD_PRICE,
    ADD_TITLE,
    ADD_USERNAME, GET_ITEM, GET_ITEMS,
    GET_USER
} from "./actionTypes";

export const addUsername = value => {
    return {type: ADD_USERNAME, value};
};
export const addDisplayName = value => {
    return {type: ADD_DISPLAY_NAME, value};
};
export const addPhoneNumber = value => {
    return {type: ADD_PHONE_NUMBER, value};
};
export const addPassword = value => {
    return {type: ADD_PASSWORD, value};
};

export const fetchUser = value => {
    return {type: GET_USER, value};
};

export const addTitle = value => {
    return {type: ADD_TITLE, value};
};
export const addDescription = value => {
    return {type: ADD_DESCRIPTION, value};
};
export const addPrice = value => {
    return {type: ADD_PRICE, value};
};
export const addCategory = value => {
    return {type: ADD_CATEGORY, value};
};
export const addImage = value => {
    return {type: ADD_IMAGE, value};
};
export const addImageName = value => {
    return {type: ADD_IMAGE_NAME, value};
};

export const getItems = value => {
    return {type: GET_ITEMS, value};
};
export const getItem = value => {
    return {type: GET_ITEM, value};
};

export const fetchItems = (category) => {
    return async dispatch => {
        if (category) {
            try{
                const response = await axios.get('http://localhost:8000/items?category=' + category);
                dispatch(getItems(response.data));
            } catch (e) {
                console.log(e);
            }
        } else {
            try {
                const response = await axios.get('http://localhost:8000/items');
                dispatch(getItems(response.data));
            } catch (e) {
                console.log(e);
            }
        }
    };
};

export const fetchItem = (item) => {
    return async dispatch => {
        try {
            const response = await axios.get('http://localhost:8000/items/' + item);
            dispatch(getItem(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const deleteItem = async (header, item) => {
    try {
        await axios.delete('http://localhost:8000/items/' + item, {headers: header});
    } catch (e) {
        console.log(e);
    }
};

export const addNewItem = async (header, item) => {
    try {
        await axios.post('http://localhost:8000/items', item, {headers: header});
    } catch (e) {
        console.log(e);
    }
};

export const registerUser = (data) => {
    return async dispatch => {
        try {
            const response = await axios.post('http://localhost:8000/users/sessions', data);
            dispatch(fetchUser(response.data));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(fetchUser(e.response.data));
            }
        }
    };
};

export const createUser = (data) => {
    return async dispatch => {
        try {
            const response = await axios.post('http://localhost:8000/users', data);
            dispatch(fetchUser(response.data))
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(fetchUser(e.response.data));
            }
        }
    };
};

export const logOutUser = () => {
    return async (dispatch, getState) => {
        const token = getState().user.user.token;
        const headers = {"Authentication": token};
        await axios.delete('http://localhost:8000/users/sessions', {headers});
        dispatch(fetchUser(null));
    };
};