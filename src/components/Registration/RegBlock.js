import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {NavLink} from "react-router-dom";
import {logOutUser} from "../../store/actions";

const RegBlock = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.user.user);

    const logOut = () => {
        dispatch(logOutUser());
    };

    if (user && user.username !== undefined) {
        return (
            <span className="nav">
                <NavLink
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer',
                        marginRight: 5
                    }}
                    to="/createItem"
                >
                    Add new item
                </NavLink>
                |
                <NavLink
                    onClick={logOut}
                    style={{
                        color: 'black',
                        textDecoration: 'underline',
                        cursor: 'pointer',
                        marginLeft: 5
                    }}
                    to="/"
                >
                    {user.displayName}
                </NavLink>
            </span>
        )
    } else {
        return (
            <span className="nav">
            <NavLink
                style={{
                    color: 'black',
                    textDecoration: 'underline',
                    cursor: 'pointer',
                    marginRight: 5,
                }}
                to="/register"
            >
                Register
            </NavLink>
                |
            <NavLink
                style={{
                    color: 'black',
                    textDecoration: 'underline',
                    cursor: 'pointer',
                    marginLeft: 5,
                }}
                to="/login"
            >
                Login
            </NavLink>
        </span>
        );
    }
};

export default RegBlock;