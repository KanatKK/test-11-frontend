import {GET_ITEM, GET_ITEMS} from "../actionTypes";

const initialState = {
    items: null,
    item: null,
};

const getItemsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ITEMS:
            return {...state, items: action.value};
        case GET_ITEM:
            return {...state, item: action.value};
        default:
            return state;
    }
};

export default getItemsReducer;