export const GET_USER = "GET_USER";

export const ADD_USERNAME = "ADD_USERNAME";
export const ADD_PASSWORD = "ADD_PASSWORD";
export const ADD_DISPLAY_NAME = "ADD_DISPLAY_NAME";
export const ADD_PHONE_NUMBER = "ADD_PHONE_NUMBER";

export const ADD_TITLE = "ADD_TITLE";
export const ADD_DESCRIPTION = "ADD_DESCRIPTION";
export const ADD_PRICE = "ADD_PRICE";
export const ADD_CATEGORY = "ADD_CATEGORY";
export const ADD_IMAGE = "ADD_IMAGE";
export const ADD_IMAGE_NAME = "ADD_IMAGE_NAME";

export const GET_ITEMS = "GET_ITEMS";
export const GET_ITEM = "GET_ITEM";