import {applyMiddleware, combineReducers, createStore} from "redux";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import thunkMiddleware from "redux-thunk";
import getUserReducer from "./reducers/getUserReducer";
import addUserReducer from "./reducers/addUserReducer";
import addItemReducer from "./reducers/addItemReducer";
import getItemsReducer from "./reducers/getItemsReducer";

const rootReducers = combineReducers({
    user: getUserReducer,
    addUser: addUserReducer,
    items: getItemsReducer,
    addItem: addItemReducer,
});

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducers, persistedState, applyMiddleware(thunkMiddleware));

store.subscribe(() => {
    saveToLocalStorage({
        user: {
            user: store.getState().user.user
        }
    });
});

export default store;