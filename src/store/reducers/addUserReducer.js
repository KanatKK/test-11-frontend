import {ADD_DISPLAY_NAME, ADD_PASSWORD, ADD_PHONE_NUMBER, ADD_USERNAME} from "../actionTypes";

const initialState = {
    username: "",
    password: "",
    displayName: "",
    phoneNumber: "",
};

const addUserReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_USERNAME:
            return {...state, username: action.value};
        case ADD_PASSWORD:
            return {...state, password: action.value};
        case ADD_DISPLAY_NAME:
            return {...state, displayName: action.value};
        case ADD_PHONE_NUMBER:
            return {...state, phoneNumber: action.value};
        default:
            return state;
    }
};

export default addUserReducer;