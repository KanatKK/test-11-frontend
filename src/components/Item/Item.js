import React, {useEffect} from 'react';
import RegBlock from "../Registration/RegBlock";
import {useDispatch, useSelector} from "react-redux";
import {deleteItem, fetchItem} from "../../store/actions";
import Spinner from "../Spinner/Spinner";

const Item = props => {
    const dispatch = useDispatch();
    const item = useSelector(state => state.items.item);
    const user = useSelector(state => state.user.user);

    useEffect(() => {
        dispatch(fetchItem(props.match.params.id));
    },[dispatch, props.match.params.id]);

    const deleteItemHandler = async (event) => {
        if (user !== null) {
            const headers = {
                'Authentication': user.token,
            };
            await deleteItem(headers, event.target.id);
            props.history.push("/");
        }
    };

    if (item !== null) {
        return (
            <div className="container">
                <header><h2>Flea market</h2><RegBlock/></header>
                <div className="itemContent">
                    <div className="image">
                        <img
                            src={'http://localhost:8000/uploads/' + item.image}
                            style={{width: 300,height: 300}}
                            alt="Product"
                        />
                    </div>
                    <div className="info">
                        <h4>{item.title}</h4>
                        <p>About item:<span className="description">{item.description}</span></p>
                        <p>Category: <span className="category">{item.category}</span></p>
                        <p>Price: <span className="price">{item.price}$</span></p>
                        <p className="phoneNumber">Number: 0{item.author.phoneNumber}</p>
                        <p>Seller: <span className="seller">{item.author.name}</span></p>
                    </div>
                </div>
                { user &&
                    user.username === item.author.username &&
                    <button
                        type="button" id={item._id} className="deleteBtn"
                        onClick={deleteItemHandler}
                    >
                        Delete item
                    </button>
                }
            </div>
        );
    } else {
        return (
            <div className="container">
                <header><h2>Flea market</h2><RegBlock/></header>
                <Spinner/>
            </div>
        );
    }
};

export default Item;