import {ADD_CATEGORY, ADD_DESCRIPTION, ADD_IMAGE, ADD_IMAGE_NAME, ADD_PRICE, ADD_TITLE} from "../actionTypes";

const initialState = {
    title: "",
    description: "",
    price: null,
    category: "Cars",
    image: null,
    imageName: "Choose an image."
};

const addItemReducer = (state= initialState, action) => {
    switch (action.type) {
        case ADD_TITLE:
            return {...state, title: action.value};
        case ADD_DESCRIPTION:
            return {...state, description: action.value};
        case ADD_PRICE:
            return {...state, price: action.value};
        case ADD_CATEGORY:
            return {...state, category: action.value};
        case ADD_IMAGE:
            return {...state, image: action.value};
        case ADD_IMAGE_NAME:
            return {...state, imageName: action.value};
        default:
            return state;
    }
};

export default addItemReducer;