import React, {useEffect} from 'react';
import RegBlock from "../../components/Registration/RegBlock";
import {useDispatch, useSelector} from "react-redux";
import {fetchItems} from "../../store/actions";
import Spinner from "../../components/Spinner/Spinner";
import Items from "../../components/Item/Items";

const MainPage = () => {
    const dispatch = useDispatch();
    const items = useSelector(state => state.items.items);

    useEffect(() => {
        dispatch(fetchItems());
    }, [dispatch]);

    const getAllItems = () => {
        dispatch(fetchItems());
    };

    const getItemsByCategory = (event) => {
        dispatch(fetchItems(event.target.value));
    };

    if (items !== null) {
        const itemsList = items.map((item, index) => {
            return (
                <Items
                    key={index} id={item._id} title={item.title}
                    price={item.price} image={item.image}
                />
            );
        });
        return (
            <div className="container">
                <header><h2>Flea market</h2><RegBlock/></header>
                <div className="catalog">
                    <div className="categories">
                        <ul>
                            <li>
                                <button
                                    className="categoryBtn"
                                    onClick={getAllItems}
                                >
                                    All items
                                </button>
                            </li>
                            <li>
                                <button
                                    className="categoryBtn" value="Cars"
                                    onClick={getItemsByCategory}
                                >
                                    Cars
                                </button>
                            </li>
                            <li>
                                <button
                                    className="categoryBtn" value="Computers"
                                    onClick={getItemsByCategory}
                                >
                                    Computers
                                </button>
                            </li>
                            <li>
                                <button
                                    className="categoryBtn" value="Other"
                                    onClick={getItemsByCategory}
                                >
                                    Other
                                </button>
                            </li>
                        </ul>
                    </div>
                    <div className="items">
                        {itemsList}
                    </div>
                </div>
            </div>
        );
    } else {
        return (
            <div className="container">
                <header><h2>Flea market</h2><RegBlock/></header>
                <Spinner/>
            </div>
        );
    }
};

export default MainPage;