import React from 'react';
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {addPassword, addUsername, registerUser} from "../../store/actions";

const Login = props => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.user.user);
    const username = useSelector(state => state.addUser.username);
    const password = useSelector(state => state.addUser.password);
    const userData = useSelector(state=> state.addUser);

    const addUserNameHandler = event => {
        dispatch(addUsername(event.target.value));
    };

    const addPasswordHandler = event => {
        dispatch(addPassword(event.target.value));
    };

    const registerUserHandler = () => {
        dispatch(registerUser(userData));
        dispatch(addUsername(""));
        dispatch(addPassword(""));
    };

    if (user !== null && user.username) {
        props.history.push("/");
    }

    return (
        <div className="container">
            <header>
                <h2>Login</h2>
                <span className="nav">
                    <NavLink
                        style={{
                            color: 'black',
                            textDecoration: 'underline',
                            cursor: 'pointer'
                        }}
                        to="/"
                    >
                        MainPage
                    </NavLink>
                </span>
            </header>
            <div className="registration">
                <input
                    type="text" className="registrationField"
                    placeholder="User Name" value={username}
                    onChange={addUserNameHandler}
                />
                <input
                    type="text" className="registrationField"
                    placeholder="Password" value={password}
                    onChange={addPasswordHandler}
                />
                <button type="button" onClick={registerUserHandler} className="regButton">Login</button>
                {user && <span className="error">{user.error}</span>}
            </div>
        </div>
    );
};

export default Login;